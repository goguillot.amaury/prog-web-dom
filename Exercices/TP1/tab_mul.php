<?php
function cree_table($i = 10, $j = 10, $q = 3) {
    //i colonnes, j lignes, q surligné
    $res = "<table> <thead> <tr> ";
    $res .= "<th> </th>";
    for ($k = 0; $k<= $i; $k ++) {
        $res .= "<th>".$k."</th>";
    }

    $res .= "</thead> <tbody";
    for ($p = 0; $p <=$j; $p++){
        if ($p == $q) 
        {
            $res.= "<tr class='surlignee'> <th>".$p."</th>"; 
        }
        else $res.= "<tr> <th>".$p."</th>";
        for ($k = 0; $k<= $i; $k ++) {
            $res .= "<td>".$k*$p."</td>";
        }
        $res.="</tr>";
    }
    $res.="</tbody> </table>";
    return $res;
}

$i = 10;
$j = 10;
$q = 3;
if(isset($_GET["i"])) $i = $_GET["i"];

if(isset($_GET["j"])) $j = $_GET["j"];

if(isset($_GET["q"])) $q = $_GET["q"];

echo cree_table($i,$j,$q);
?>

<html>
<head>
<title>table multiplication</title>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="elements.css" />
</head>
</html>